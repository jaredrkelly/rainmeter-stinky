# replaced by regex calls in the skins themselves. keeping in case required for something else down the track
function Get-SubRedditJson($webClient, $sub, $debug) {
    $url = "https://www.reddit.com/r/$sub/.json"
    $result=$webClient.DownloadString($url) | ConvertFrom-Json
    if ($debug) {
        write-host $url
        write-host $result 
    }
    return $result
}