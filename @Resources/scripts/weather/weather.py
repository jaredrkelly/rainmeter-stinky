import json, os, requests
from datetime import datetime



# https://api.darksky.net/forecast/0123456789abcdef9876543210fedcba/42.3601,-71.0589

## CONFIG ##
debug = False
config_file = "config.json"
base_url = "https://api.darksky.net/forecast"
parameters = "?units=auto&exclude=minutely,hourly,alerts,flags"
folder = os.path.dirname(os.path.realpath(__file__)) # directory of this script


## CODE ##





def getConfig(the_file):
    with open(the_file, 'rb') as config:
        return json.load(config)

def getWeather(lat, lng, key):
    url = base_url + "/" + key + "/" + str(lat) + "," + str(lng) + parameters
    if debug:
        print(f"url is: {url}")
    response = json.loads(requests.get(url).text)
    if debug:
        print(response)
    return response

def printCurrentWeather(current):
    print(f"<currentSummary>{current['summary']}</currentSummary>")
    print(f"<currentIcon>{current['icon']}</currentIcon>")
    print(f"<currentTemp>{current['temperature']}</currentTemp>")

def printForecast(forecast):
    for (idx, day) in enumerate(forecast["data"]):
        print(f"<day{idx}>{datetime.fromtimestamp(day['time']).strftime('%a')}</day{idx}>")
        print(f"<min{idx}>{day['temperatureLow']}</min{idx}>")
        print(f"<max{idx}>{day['temperatureHigh']}</max{idx}>")
        print(f"<icon{idx}>{day['icon']}</icon{idx}>")

def main():
    config = getConfig(os.path.join(folder,config_file))
    if debug:
        print(f"config contents are:\n{config}")
    response = getWeather(config["lat"], config["lng"], config["apiKey"])
    printCurrentWeather(response["currently"])
    printForecast(response["daily"])

if __name__ == '__main__':
    main()