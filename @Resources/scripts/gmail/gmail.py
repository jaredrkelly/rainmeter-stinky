from __future__ import print_function
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import os, pickle

# BEFORE USE
# 1. Install these libs
#   pip install google-api-python-client google-auth-httplib2 google-auth-oauthlib
# 2. On first run you will need to authorise the app to read your emails. Your 
# browser should open the link by default 

## CONFIG ##
SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
query = "label:inbox is:unread"
# OR
#query = "is:important label:inbox is:unread"
max_messages = 10

outFile = "unread_msgs.txt"
tokenFile = "gmail.token"
credFile = "credentials.json"
folder = os.path.dirname(os.path.realpath(__file__)) # directory of this script

debug = False   # change to True to print out stuff

## CODE ##

# get creds/token and authorise
def getCreds(token_file, cred_file):
    creds = None
    if os.path.exists(token_file):
        with open(token_file, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(cred_file, SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(token_file, 'wb') as token:
            pickle.dump(creds, token)
    return creds
    
def getMessageIds(service, qry):
    results = service.users().messages().list(userId='me',q=qry).execute()
    if debug:
        print(results)
    if not "messages" in results:
        return []
    return results["messages"]

def printMessagesFromIds(messageIds, service):
    if len(messageIds) == 0:
        print("<gmail>No new emails.</gmail>")
        exit()
    msgCount = 1
    for message in messageIds:
        if msgCount > max_messages:
            break
        message_id = message["id"]
        message_details = service.users().messages().get(userId='me', id=message_id, format="metadata").execute()
        for header in message_details["payload"]["headers"]:
            if header["name"] == "Subject":
                print("<gmail>%s</gmail>" % (header["value"].encode("ascii", "ignore").decode()))
                msgCount+=1

def main():
    creds = getCreds(os.path.join(folder,tokenFile), os.path.join(folder,credFile))
    service = build('gmail', 'v1', credentials=creds)
    messageIds = getMessageIds(service, query)
    printMessagesFromIds(messageIds, service)

if __name__ == '__main__':
    main()